import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '.././../redux/actions/index';
import { BASE_URL, CLONE_BASE_URL, API_TOKEN } from '../../api/globals';
import axios from 'axios';
import PhoneNumber from '../Login/IntlTelePhone';
import DatePicker from "react-datepicker";
import moment from 'moment';
import AlertBox from '../Common/AlertBox/AlertBox';
import './AmirahClub.css';
import "react-datepicker/dist/react-datepicker.css";
import { FormattedMessage } from 'react-intl';



class AmirahClub extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fields: {
                firstName: '',
                lastName: '',
                email: '',
                countryCode: '',
                contactNumber: '',
            },
            errors: {},
            isPhoneValid: false,
            data: {},
            birthDate: '',
            anniversaryDate: '',
            isVipRegSuccess: false,
            vipRegStatusMessage: '',
            datePickerBDIsOpen: false,
            datePickerWAIsOpen: false,
            alertBoxDetails: {
                status: false,
                message: '',
            }
        }
        this.changeBirthDate = this.changeBirthDate.bind(this);
        this.changeAnniversaryDate = this.changeAnniversaryDate.bind(this);

        this.openDatePickerBD = this.openDatePickerBD.bind(this);
        this.openDatePickerWA = this.openDatePickerWA.bind(this);
    }

    changeBirthDate = (date) => {
        //console.log(date);
        this.setState({
            birthDate: date
        });
        this.openDatePickerBD();
    }

    changeAnniversaryDate = (date) => {
        this.setState({
            anniversaryDate: date
        });
        this.openDatePickerWA();
    }

    handleValidation = () => {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        //Name
        if (!fields["firstName"]) {
            formIsValid = false;
            errors["firstName"] = "Cannot be empty";
        }

        if (!fields["lastName"]) {
            formIsValid = false;
            errors["lastName"] = "Cannot be empty";
        }

        if (typeof fields["firstName"] !== "undefined") {
            if (!fields["firstName"].match(/^[a-zA-Z]+$/)) {
                formIsValid = false;
                errors["firstName"] = "Only letters";
            }
        }

        if (typeof fields["lastName"] !== "undefined") {
            if (!fields["lastName"].match(/^[a-zA-Z]+$/)) {
                formIsValid = false;
                errors["lastName"] = "Only letters";
            }
        }

        //Email
        if (!fields["email"]) {
            formIsValid = false;
            errors["email"] = "Cannot be empty";
        }

        if (typeof fields["email"] !== "undefined") {
            let lastAtPos = fields["email"].lastIndexOf('@');
            let lastDotPos = fields["email"].lastIndexOf('.');

            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') == -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
                formIsValid = false;
                errors["email"] = "Email is not valid";
            }
        }

        if (!(this.state.isPhoneValid)) {
            formIsValid = false;
            errors["contactNumber"] = "Enter valid Phone number";
        }

        this.setState({ errors: errors });
        return formIsValid;
    }

    registerAmirah = (e) => {
        e.preventDefault();
        console.log(this.state);
        if (this.handleValidation()) {
            this.registerUser();
        }
    }

    handleChange = (field, e) => {
        //console.log(field, e.target.value);

        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({ fields });
        //console.log(this.state);
    }

    openDatePickerBD() {
        this.setState({
            datePickerBDIsOpen: !this.state.datePickerBDIsOpen,
        });
    };

    openDatePickerWA() {
        this.setState({
            datePickerWAIsOpen: !this.state.datePickerWAIsOpen,
        });
    };

    registerUser = () => {
        // console.log('calling register Api : ', this.state);



        const reqdata = {
            firstname: this.state.fields.firstName,
            lastname: this.state.fields.lastName,
            countryCode: this.state.fields.countryCode,
            phoneNumber: this.state.fields.countryCode + this.state.fields.contactNumber,
            email: this.state.fields.email,
            Dob: this.state.birthDate,
            Dowa: this.state.anniversaryDate,
            storeid: this.props.globals.currentStore,
        }

        console.log('reqdata: ', reqdata);

        const API = axios.create({
            baseURL: CLONE_BASE_URL,
            headers: { Authorization: `Bearer ${API_TOKEN}`, "Content-Type": "application/json" }
        });

        API.post('/amirahclub', reqdata).then(res => {
            //console.log(res);
            //alert(res.data.message);
            this.setState({
                ...this.state,
                alertBoxDetails: {
                    status: true,
                    message: res.data.message,
                }
            })
        })

    }

    validateRegister = () => {
        //console.log('Validate Registration');
        const loginData = this.state.data;

        if (loginData.status) {

           // alert(loginData.data.message);
            this.setState({
                ...this.state,
                alertBoxDetails: {
                    status: true,
                    message: loginData.data.message,
                }
            })
            
        } else {
            this.setState({
                ...this.state,
                alertBoxDetails: {
                    status: true,
                    message: loginData.data.message,
                }
            })
            // alert(loginData.data.message);
        }
    }

    contactNumber = (status, value, countryData, number, id) => {
        //console.log('from parent',status, value, countryData, number, id)
        if (status) {
            let fields = this.state.fields;
            fields['countryCode'] = countryData.dialCode;
            fields['contactNumber'] = value;
            this.setState({ fields, isPhoneValid: true });
            //console.log(this.state);
        } else {
            this.setState({ isPhoneValid: false })
        }
    }

    divOnFocus = (e) => {
        e.currentTarget.className = 't-Form-fieldContainer t-Form-fieldContainer--floatingLabel apex-item-wrapper apex-item-wrapper--text-field is-active';
    }

    divOnBlure = (e) => {

        if ((e.target.value == null) || (e.target.value == '')) {
            e.currentTarget.className = 't-Form-fieldContainer t-Form-fieldContainer--floatingLabel apex-item-wrapper apex-item-wrapper--text-field';
        } else {
            e.currentTarget.className = 't-Form-fieldContainer t-Form-fieldContainer--floatingLabel apex-item-wrapper apex-item-wrapper--text-field is-active';
        }
    }

    closeErrorBox = () => {
        this.setState({
            ...this.state,
            alertBoxDetails: {
                status: false,
                message: ''
            }
        })
        console.log('Close alert Box Parent');
    }

    render() {

        let alertBox = null;

        if (this.state.alertBoxDetails.status) {
            alertBox = <AlertBox
                message={this.state.alertBoxDetails.message}
                alertBoxStatus={this.state.alertBoxDetails.status}
                closeBox={this.closeErrorBox} />
        }

        const errorsObj = this.state.errors;
        //console.log(errorsObj);

        let firstNameInputField = <div className="t-Form-inputContainer"><div className="t-Form-itemWrapper">
            <input type="text" id="P11_FIRST_NAME" name="P11_FIRST_NAME" required className="text_field apex-item-text" onChange={this.handleChange.bind(this, "firstName")} value={this.state.fields["firstName"]} size="30" maxLength="100" /></div><span id="P11_FIRST_NAME_error_placeholder" className="a-Form-error" data-template-id="33610259035469734_ET" /></div>;

        let LastNameInputField = <div className="t-Form-inputContainer"><div className="t-Form-itemWrapper">
            <input type="text" id="P11_LAST_NAME" name="P11_LAST_NAME" className="text_field apex-item-text "
                onChange={this.handleChange.bind(this, "lastName")} value={this.state.fields["lastName"]} size="30" maxLength="100" /></div><span id="P11_LAST_NAME_error_placeholder" className="a-Form-error" data-template-id="33610259035469734_ET"></span></div>;


        let emailInputField = <div className="t-Form-inputContainer"><div className="t-Form-itemWrapper">
            <input type="email" id="P11_EMAIL" name="P11_EMAIL" className="text_field apex-item-text " onChange={this.handleChange.bind(this, "email")} value={this.state.fields["email"]} size="30" maxLength="100" />
        </div><span id="P11_EMAIL_error_placeholder" className="a-Form-error" data-template-id="33610259035469734_ET"></span></div>;


        let contactNumberInputField = null;

        if ('firstName' in errorsObj) {
            firstNameInputField =
                <div className="t-Form-inputContainer">
                    <div className="t-Form-itemWrapper">
                        <input type="text" id="P11_FIRST_NAME" name="P11_FIRST_NAME" required className="text_field apex-item-text apex-page-item-error" onChange={this.handleChange.bind(this, "firstName")} value={this.state.fields["firstName"]} size={30} maxLength={100} aria-describedby="P11_FIRST_NAME_error P11_FIRST_NAME_error P11_FIRST_NAME_error P11_FIRST_NAME_error" aria-invalid="true" data-old-aria-describedby="P11_FIRST_NAME_error P11_FIRST_NAME_error P11_FIRST_NAME_error" /></div><span id="P11_FIRST_NAME_error_placeholder" className="a-Form-error u-visible" data-template-id="33610259035469734_ET"><span className="t-Form-error"><div id="P11_FIRST_NAME_error"><FormattedMessage id="Amirah.FirstNameError" defaultMessage="Enter first name!" /></div></span></span></div>;
        }

        if ('lastName' in errorsObj) {
            LastNameInputField = <div className="t-Form-inputContainer"><div className="t-Form-itemWrapper"><input type="text" id="P11_LAST_NAME" name="P11_LAST_NAME" className="text_field apex-item-text apex-page-item-error" onChange={this.handleChange.bind(this, "lastName")} value={this.state.fields["lastName"]} size={30} maxLength={100} aria-describedby="P11_LAST_NAME_error P11_LAST_NAME_error P11_LAST_NAME_error" aria-invalid="true" data-old-aria-describedby="P11_LAST_NAME_error P11_LAST_NAME_error" /></div><span id="P11_LAST_NAME_error_placeholder" className="a-Form-error u-visible" data-template-id="33610259035469734_ET"><span className="t-Form-error"><div id="P11_LAST_NAME_error"><FormattedMessage id="Amirah.LastNameError" defaultMessage="Enter Last name!" /></div></span></span></div>;
        }

        if ('contactNumber' in errorsObj) {
            contactNumberInputField = <span id="P11_PHONE_error_placeholder" class="a-Form-error u-visible" data-template-id="33610259035469734_ET"><span class="t-Form-error"><div id="P11_PHONE_error"><FormattedMessage id="Amirah.PhoneNumber" defaultMessage="Enter your phone number!" /></div></span></span>
        }
        if ('email' in errorsObj) {
            emailInputField = <div className="t-Form-inputContainer"><div className="t-Form-itemWrapper"><input type="email" id="P11_EMAIL" name="P11_EMAIL" required className="text_field apex-item-text apex-page-item-error" onChange={this.handleChange.bind(this, "email")} value={this.state.fields["email"]} size="30" maxLength="100" aria-describedby="P11_EMAIL_error" aria-invalid="true" /></div><span id="P11_EMAIL_error_placeholder" className="a-Form-error u-visible" data-template-id="33610259035469734_ET"><span className="t-Form-error"><div id="P11_EMAIL_error"><FormattedMessage id="Amirah.EmailIdError" defaultMessage="Enter your email ID!" /></div></span></span></div>;
        }


        return (<>
        {alertBox}
        <div className="t-Body-contentInner">
            
            <div className="AmirahClub container">
                <div className="row">
                    <div className="col col-12 apex-col-auto">
                        <div className="t-Region containers  t-Region--noPadding t-Region--removeHeader t-Region--noBorder t-Region--hiddenOverflow margin-top-lg" id="R228453124910656188">
                            <div className="t-Region-header">
                                <div className="t-Region-headerItems t-Region-headerItems--title">
                                    <span className="t-Region-headerIcon"><span className="t-Icon " aria-hidden="true" /></span>
                                    <h2 className="t-Region-title" id="R228453124910656188_heading">main</h2>
                                </div>
                                <div className="t-Region-headerItems t-Region-headerItems--buttons"><span className="js-maximizeButtonContainer" /></div>
                            </div>
                            <div className="t-Region-bodyWrap" >
                                <div className="t-Region-buttons t-Region-buttons--top">
                                    <div className="t-Region-buttons-left" />
                                    <div className="t-Region-buttons-right" />
                                </div>
                                <div className="t-Region-body">
                                    <input type="hidden" id="P11_PAGE_TITLE" name="P11_PAGE_TITLE" value="Amirah Club - Exciting Offers for all Our Favorite Registered Customers" /><input type="hidden" id="P11_PAGE_DESC" name="P11_PAGE_DESC" value="Amirah Club - Grab tour special exciting offers in nayomi only for Amirah club registered customers" /><input type="hidden" id="P11_EM" name="P11_EM" value={null} /><input type="hidden" data-for="P11_EM" value="UUkAzrKhSFmw-iikY14bxv-rQeZNFiuVfVPWkU-l_Sb4SlrYQcQh-4DP8c1UeZyS5OVDJLbvxSe-4l752aBIhg" />
                                    <div className="container">
                                        <div className="row">
                                            <div className="col col-4 ">
                                                <div className="t-Region t-Region--removeHeader t-Region--noBorder t-Region--hiddenOverflow margin-right-none" id="R228453143601656189">
                                                    <div className="t-Region-header">
                                                        <div className="t-Region-headerItems t-Region-headerItems--title">
                                                            <span className="t-Region-headerIcon"><span className="t-Icon " aria-hidden="true" /></span>
                                                            <h2 className="t-Region-title" id="R228453143601656189_heading">pic</h2>
                                                        </div>
                                                        <div className="t-Region-headerItems t-Region-headerItems--buttons"><span className="js-maximizeButtonContainer" /></div>
                                                    </div>
                                                    <div className="t-Region-bodyWrap">
                                                        <div className="t-Region-buttons t-Region-buttons--top">
                                                            <div className="t-Region-buttons-left" />
                                                            <div className="t-Region-buttons-right" />
                                                        </div>
                                                        <div className="t-Region-body">
                                                            <center>
                                                                <picture>
                                                                    <source srcSet="https://storage.googleapis.com/nay/qr/NAY_QR_WL_EN.jpg" media="(min-width:1220px)" />
                                                                    <source media="(max-width: 1024px)" srcSet="https://storage.googleapis.com/nay/qr/NAY_QR_WP_EN.jpg" />
                                                                    <img src="https://storage.googleapis.com/nay/qr/NAY_QR_WL_EN.jpg" style={{ width: '100%', height: '100%' }} />
                                                                </picture></center>
                                                        </div>
                                                        <div className="t-Region-buttons t-Region-buttons--bottom">
                                                            <div className="t-Region-buttons-left" />
                                                            <div className="t-Region-buttons-right" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><div className="col col-8 apex-col-auto">
                                                <div className="t-Region t-Region--noPadding t-Region--removeHeader t-Region--noBorder t-Region--scrollBody" id="R503961702148051046 UB">
                                                    <div className="t-Region-header">
                                                        <div className="t-Region-headerItems t-Region-headerItems--title">
                                                            <span className="t-Region-headerIcon"><span className="t-Icon " aria-hidden="true" /></span>
                                                            <h2 className="t-Region-title" id="UB_heading">upper banner</h2>
                                                        </div>
                                                        <div className="t-Region-headerItems t-Region-headerItems--buttons"><span className="js-maximizeButtonContainer" /></div>
                                                    </div>
                                                    <div className="t-Region-bodyWrap">
                                                        <div className="t-Region-buttons t-Region-buttons--top">
                                                            <div className="t-Region-buttons-left" />
                                                            <div className="t-Region-buttons-right" />
                                                        </div>
                                                        <div className="t-Region-body">
                                                            <div className="h-hidden-mobile" style={{ padding: '20px 16px', display: 'flex', alignItems: 'center' }}><i className="amirah-logo" />
                                                                <div>
                                                                    <p style={{ fontSize: '22px', lineHeight: '16px', letterSpacing: '.1em', textTransform: 'uppercase', fontWeight: 'normal', color: '#f599bb', margin: '0 25px' }}>
                                                                        <FormattedMessage id="Amirah.headerTitle" defaultMessage="Register VIP club" />
                                                                    </p>
                                                                    <hr style={{ margin: '5px 25px' }} />
                                                                    <ul className="amirah-list">
                                                                        <li className="first"><FormattedMessage id="Amirah.VIPDiscounts" defaultMessage="VIP Discounts" /></li>
                                                                        <li><FormattedMessage id="Amirah.BirthdayTreats" defaultMessage="Birthday Treats" /></li>
                                                                        <li><FormattedMessage id="Amirah.PrivateMsg" defaultMessage="Private Events & Sales" /></li>
                                                                        <li><FormattedMessage id="Amirah.MuchMoreMsg" defaultMessage="And Much Much More.." /></li>
                                                                    </ul>
                                                                </div></div>
                                                            <div className="h-hidden-desktop">
                                                                <div style={{ padding: '20px 16px', display: 'flex' }}><i className="amirah-logom" />
                                                                    <div style={{ width: '100%', marginLeft: '9px' }}>
                                                                        <p style={{ fontSize: '15px', marginTop: '8px', fontWeight: 'normal', color: '#f599bb', borderBottom: '1px solid #f599bb', paddingBottom: '6px' }}>
                                                                            <FormattedMessage id="Amirah.headerTitle" defaultMessage="Register VIP club" />
                                                                        </p>
                                                                        <hr style={{ margin: '5px 25px' }} />
                                                                        <div className="lgText" style={{ width: '100%' }}>
                                                                            <div className="lgStyle"> <span style={{ borderRight: '1px solid #f599bb', paddingRight: '27px' }}>VIP Discounts</span></div>
                                                                            <div><span><FormattedMessage id="Amirah.BirthdayTreats" defaultMessage="Birthday Treats" /></span></div>
                                                                        </div>
                                                                        <div className="lgText" style={{ width: '100%' }}>
                                                                            <div className="lgStyle"><span style={{ borderRight: '1px solid #f599bb', paddingRight: '18px' }}>Private Events &amp; Sales</span></div>
                                                                            <div><span><FormattedMessage id="Amirah.MuchMoreMsg" defaultMessage="And Much Much More.." /></span></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="t-Region-buttons t-Region-buttons--bottom">
                                                            <div className="t-Region-buttons-left" />
                                                            <div className="t-Region-buttons-right" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="t-Region i-h480 t-Region--removeHeader t-Region--noBorder t-Region--hiddenOverflow t-Form--large t-Form--stretchInputs t-Form--labelsAbove margin-left-none" id="form">
                                                    <div className="t-Region-header">
                                                        <div className="t-Region-headerItems t-Region-headerItems--title">
                                                            <span className="t-Region-headerIcon"><span className="t-Icon " aria-hidden="true" /></span>
                                                            <h2 className="t-Region-title" id="form_heading">form</h2>
                                                        </div>
                                                        <div className="t-Region-headerItems t-Region-headerItems--buttons"><span className="js-maximizeButtonContainer" /></div>
                                                    </div>
                                                    <div className="t-Region-bodyWrap">
                                                        <div className="t-Region-buttons t-Region-buttons--top">
                                                            <div className="t-Region-buttons-left" />
                                                            <div className="t-Region-buttons-right" />
                                                        </div>
                                                        <div className="t-Region-body" style={{ overflow: 'inherit' }}>
                                                            <div className="container" style={{ overflow: 'inherit' }}>
                                                                <div className="row">
                                                                    <div className="col col-6 apex-col-auto">
                                                                        <div className="t-Form-fieldContainer t-Form-fieldContainer--floatingLabel is-required apex-item-wrapper apex-item-wrapper--text-field" id="P11_FIRST_NAME_CONTAINER" onFocus={(e) => this.divOnFocus(e)}
                                                                            onBlur={(e) => this.divOnBlure(e)}><div className="t-Form-labelContainer">
                                                                                <label htmlFor="P11_FIRST_NAME" id="P11_FIRST_NAME_LABEL" className="t-Form-label">
                                                                                    <FormattedMessage id="Form.FirstName" defaultMessage="First Name" />
                                                                                    <span className="u-VisuallyHidden">(Value Required)</span>
                                                                                </label>
                                                                            </div>

                                                                            {firstNameInputField}

                                                                        </div><input type="hidden" id="P11_NAME" name="P11_NAME" value={null} /><input type="hidden" id="P11_EMAILID" name="P11_EMAILID" value={null} /><input type="hidden" id="P11_STATUS" name="P11_STATUS" value={null} />
                                                                    </div><div className="col col-6 apex-col-auto">
                                                                        <div className="t-Form-fieldContainer t-Form-fieldContainer--floatingLabel is-required apex-item-wrapper apex-item-wrapper--text-field" id="P11_LAST_NAME_CONTAINER" onFocus={(e) => this.divOnFocus(e)}
                                                                            onBlur={(e) => this.divOnBlure(e)}><div className="t-Form-labelContainer">
                                                                                <label htmlFor="P11_LAST_NAME" id="P11_LAST_NAME_LABEL" className="t-Form-label">
                                                                                    <FormattedMessage id="Form.LastName" defaultMessage="Last Name" /><span className="u-VisuallyHidden">(Value Required)</span></label>
                                                                            </div>


                                                                            {LastNameInputField}


                                                                        </div>
                                                                    </div>
                                                                </div><div className="row">
                                                                    <div className="col col-6 apex-col-auto">
                                                                        <div className="t-Form-fieldContainer t-Form-fieldContainer--floatingLabel is-required apex-item-wrapper plugin-intltelinput-www.jqueryscript.net" id="P11_PHONE_CONTAINER">
                                                                            <div className="t-Form-labelContainer">
                                                                                <label htmlFor="P11_PHONE" id="P11_PHONE_LABEL" className="t-Form-label">
                                                                                    <span className="u-VisuallyHidden">(Value Required)</span>
                                                                                </label>
                                                                            </div>

                                                                            <div className='t-Form-inputContainer NumberDropdown'>
                                                                                <PhoneNumber changed={this.contactNumber} />
                                                                                {contactNumberInputField}
                                                                            </div>
                                                                        </div>

                                                                    </div><div className="col col-6 apex-col-auto">
                                                                        <div className="t-Form-fieldContainer t-Form-fieldContainer--floatingLabel is-required apex-item-wrapper apex-item-wrapper--text-field" id="P11_EMAIL_CONTAINER" onFocus={(e) => this.divOnFocus(e)}
                                                                            onBlur={(e) => this.divOnBlure(e)}><div className="t-Form-labelContainer">
                                                                                <label htmlFor="P11_EMAIL" id="P11_EMAIL_LABEL" className="t-Form-label">
                                                                                    <FormattedMessage id="Form.Email" defaultMessage="Email" /><span className="u-VisuallyHidden">(Value Required)</span></label>
                                                                            </div>


                                                                            {emailInputField}

                                                                        </div>
                                                                    </div>
                                                                </div><div className="row">
                                                                    <div className="col col-6 apex-col-auto Calender">
                                                                        <div className="t-Form-fieldContainer t-Form-fieldContainer--floatingLabel apex-item-wrapper apex-item-wrapper--date-picker is-required is-active" id="P11_DOB_CONTAINER">

                                                                            <DatePicker
                                                                                id="BD-DP"
                                                                                selected={this.state.birthDate}
                                                                                onChange={this.changeBirthDate}
                                                                                placeholderText="Birthday"
                                                                                peekNextMonth
                                                                                showMonthDropdown
                                                                                showYearDropdown
                                                                                dropdownMode="select"
                                                                                open={this.state.datePickerBDIsOpen}
                                                                                onClickOutside={this.openDatePickerBD}
                                                                                className=" datepicker apex-item-text apex-item-datepicker hasDatepicker"
                                                                            />
                                                                            <label></label>

                                                                            <button type="button" onClick={this.openDatePickerBD} className="ui-datepicker-trigger a-Button a-Button--calendar"><span className="far fa-calendar-alt" /><span className="u-VisuallyHidden">Popup Calendar: Birthday<span /></span></button>
                                                                            <span className="u-VisuallyHidden" id="P11_DOB_format_help">For example, 04/01/2019</span></div><span id="P11_DOB_error_placeholder" className="a-Form-error" data-template-id="33609965712469734_ET" />
                                                                    </div>

                                                                    <div className="col col-6 apex-col-auto Calender">
                                                                        <div className="t-Form-fieldContainer t-Form-fieldContainer--floatingLabel apex-item-wrapper apex-item-wrapper--date-picker is-required is-active" id="P11_WA_CONTAINER">
                                                                            <DatePicker
                                                                                id="WA-DP"
                                                                                selected={this.state.anniversaryDate}
                                                                                onChange={this.changeAnniversaryDate}
                                                                                placeholderText="Wedding Anniversary"
                                                                                peekNextMonth
                                                                                showMonthDropdown
                                                                                showYearDropdown
                                                                                dropdownMode="select"
                                                                                open={this.state.datePickerWAIsOpen}
                                                                                onClickOutside={this.openDatePickerWA}

                                                                                className=" datepicker apex-item-text apex-item-datepicker hasDatepicker"
                                                                            />

                                                                            <button type="button" className="ui-datepicker-trigger a-Button a-Button--calendar" onClick={this.openDatePickerWA}><span className="far fa-calendar-alt" /><span className="u-VisuallyHidden">Popup Calendar: Wedding Anniversary<span /></span></button><span className="u-VisuallyHidden" id="P11_WA_format_help">For example, 04/01/2019</span><span id="P11_WA_error_placeholder" className="a-Form-error" data-template-id="33609965712469734_ET" /><input type="hidden" id="P11_LNG" name="P11_LNG" value="en" />
                                                                        </div></div>
                                                                </div><div className="row">
                                                                    <div className="col col-2 ">
                                                                        <span className="apex-grid-nbsp">&nbsp;</span>
                                                                    </div><div className="col col-8 ">
                                                                        <button onClick={this.registerAmirah} className="t-Button t-Button--hot t-Button--large t-Button--stretch t-Button--gapTop" type="button" id="btnRegister" style={{ zIndex: 0 }}>
                                                                            <span className="t-Button-label"><FormattedMessage id="Form.Register" defaultMessage="Register" />
                                                                            </span></button><input type="hidden" id="P11_DATE" name="P11_DATE" value="01-04-2019 10:06:29" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="t-Region-buttons t-Region-buttons--bottom">
                                                            <div className="t-Region-buttons-left" />
                                                            <div className="t-Region-buttons-right" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="t-Region form-hide  t-Region--removeHeader t-Region--noBorder t-Region--hiddenOverflow margin-right-none" id="status">
                                                    <div className="t-Region-header">
                                                        <div className="t-Region-headerItems t-Region-headerItems--title">
                                                            <span className="t-Region-headerIcon"><span className="t-Icon " aria-hidden="true" /></span>
                                                            <h2 className="t-Region-title" id="status_heading">status</h2>
                                                        </div>
                                                        <div className="t-Region-headerItems t-Region-headerItems--buttons"><span className="js-maximizeButtonContainer" /></div>
                                                    </div>
                                                    <div className="t-Region-bodyWrap">
                                                        <div className="t-Region-buttons t-Region-buttons--top">
                                                            <div className="t-Region-buttons-left" />
                                                            <div className="t-Region-buttons-right" />
                                                        </div>
                                                        <div className="t-Region-body">
                                                            <center><div style={{ lineHeight: '60px' }}><div id="icon" /> <span><h1 id="msg" /><span /></span></div></center><div className="container">
                                                                <div className="row">
                                                                    <div className="col col-2 ">
                                                                        <span className="apex-grid-nbsp">&nbsp;</span>
                                                                    </div><div className="col col-8 ">
                                                                        <button onClick={console.log('click')} className="t-Button t-Button--hot t-Button--large t-Button--stretch" type="button" id="STATBTN"><span className="t-Button-label"
                                                                        ><FormattedMessage id="Ok.text" defaultMessage="Ok" /></span></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="t-Region-buttons t-Region-buttons--bottom">
                                                            <div className="t-Region-buttons-left" />
                                                            <div className="t-Region-buttons-right" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="t-Region-buttons t-Region-buttons--bottom">
                                    <div className="t-Region-buttons-left" />
                                    <div className="t-Region-buttons-right" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div></>
        );
    }
}

const mapStateToProps = state => {
    return {
        globals: state.global,
        registration_details: state.vipReg.registerUserDetails,
    };
}

const mapDispatchToProps = dispatch => {
    return {
        onvipRegisterUserUser: (payload) => dispatch(actions.vipRegisterUser(payload)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AmirahClub);