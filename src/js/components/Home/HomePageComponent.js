import React, { Component } from 'react';
import flower from '../../../assets/images/Home/homepage-flower.png';
import ZeroItem from './Instagram';
import InstagramItems from './Instagram';
import Slider from "react-slick";
import { FormattedMessage } from 'react-intl';
import { Parallax, Background } from 'react-parallax';

class HomePageComponent extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        //console.log('this.props',this.props);
        // const instagramFeed = (this.props.home_page_data.instagram.length <= 0) ? <ZeroItem /> : <InstagramItems instagramItems={this.props.home_page_data.instagram.image_data} />
        let InstaTitle1 = '', InstaTitle2 = ''
        if (this.props.home_page_data.instagram.title) {
            InstaTitle1 = this.props.home_page_data.instagram.title.split('@')[0] + '@'
            InstaTitle2 = this.props.home_page_data.instagram.title.split('@')[1]
        }

        const settings = {
            dots: false,
            infinite: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }
            ]
        }

        return (
            <>
<div className="t-Body">
    <div className="t-Body-main" style={{ marginTop: '0px !important' }}>
        <div className="t-Body-title" id="t_Body_title" style={{ 'top': '294px' }}></div>
        <div className="t-Body-content" id="t_Body_content">
            <div id="t_Body_content_offset" style={{ 'height': '1px' }}></div> <span id="APEX_SUCCESS_MESSAGE" data-template-id="33770911730796245_S" className="apex-page-success u-hidden"></span><span id="APEX_ERROR_MESSAGE" data-template-id="33770911730796245_E" className="apex-page-error u-hidden"></span>
            <div className="t-Body-contentInner hideInMobile">
                <div className="container">
                    <div className="row">
                        <div className="col col-12 apex-col-auto">
                            <div id="pref_popup_parent"></div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col col-12 apex-col-auto homeBlock">
                            <div id="R33787481922169811" className="homePage">
                                <input type="hidden" id="P1_PAGE_TITLE" name="P1_PAGE_TITLE" value="Online Shopping in Saudi for Lingerie, Bras, Panties &amp; Nightwear at Nayomi" />
                                <input type="hidden" id="P1_PAGE_DESC" name="P1_PAGE_DESC" value="Shop lingerie online at best prices in Saudi on Nayomi. Choose from wide range of bras, panties, nightwear, shapewear &amp; loungewear for women. ✓ Free Delivery* ✓ Click &amp; Collect ✓ Cash on Delivery" />
                                <input type="hidden" id="P1_PAGE_KEYWORDS" name="P1_PAGE_KEYWORDS" value="online shopping,lingerie,bra,sexy lingerie,online shop,womens underwear,ladies underwear,womens lingerie" />
                                {/* <section className="elegance">
                                    <div className="sectionBlocks observer animate">
                                        <figure>
                                            
                                                   <img className="block1_banner1 hideInMobile" src={this.props.home_page_data.Block1.BLOCK1_BANNER1} />
                                        </figure>
                                        <div className="contentBox">
                                            <h2 data-center="transform:translateY(0%);" data-top-bottom="transform:translateY(-20%);" className="skrollable skrollable-between" style={{ transform: 'translateY(-7.44898%)' }}><span /><span /></h2>
                                            <div data-center="transform:translateY(0%);" data-top-bottom="transform:translateY(-80%);" className="skrollable skrollable-between" style={{ transform: 'translateY(-18.6802%)' }}> <a href={this.props.home_page_data.Block1.BLOCK1_URL1} className="arrowLink">
                                            <FormattedMessage id="Shop.Collection.Msg" defaultMessage="Shop Collection" />
                                            </a>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                          */}
                            </div>
                        </div>
                    </div>
                    {/* <div className="row">
                        <div className="col col-12 apex-col-auto homeBlock">
                            <div id="R33787503821169812" className="homePage">
                                <section className="romanceGift">
                                    <div className="sectionBlocks romance observer animate skrollable skrollable-before">
                                        <div className="Block1Section1">
                                            <Parallax strength={380}>
                                                <div className="Section2Height">
                                                    <div className="contentBox skrollable skrollable-before">
                                                        <h2>
                                                            {this.props.home_page_data.Block2.BLOCK2_TITLE1.split(" ").length >= 2 &&(
                                                                <>
                                                                    <span>{this.props.home_page_data.Block2.BLOCK2_TITLE1.split(" ")[0] == ""  ? this.props.home_page_data.Block2.BLOCK2_TITLE1.split(" ")[1] : this.props.home_page_data.Block2.BLOCK2_TITLE1.split(" ")[0]}</span>
                                                                    <span>{this.props.home_page_data.Block2.BLOCK2_TITLE1.split(" ")[0] == ""  ? this.props.home_page_data.Block2.BLOCK2_TITLE1.split(" ")[2] : this.props.home_page_data.Block2.BLOCK2_TITLE1.split(" ")[1]}</span>
                                                                </>    
                                                            )}
                                                            {this.props.home_page_data.Block2.BLOCK2_TITLE1.split(" ").length < 2 && (
                                                              <span>{this.props.home_page_data.Block2.BLOCK2_TITLE1}</span>  
                                                            )}
                                                        </h2>
                                                        <a href={this.props.home_page_data.Block2.BLOCK2_URL1} className="arrowLink">
                                                            <FormattedMessage id="Shop.Collection.Msg" defaultMessage="Shop Collection" />
                                                        </a>
                                                    </div>
                                                </div>
                                                <Background>
                                                    <img className="block2_banner1 hideInMobile" src={this.props.home_page_data.Block2.BLOCK2_BANNER1}/>
                                                    
                                                </Background>
                                            </Parallax>
                                        </div>
                                    </div>
                                    <div className="sectionBlocks gifting observer animate skrollable skrollable-before">
                                        <div className="Block1Section2">
                                            <Parallax strength={380}>
                                                <div className="Section2Height">
                                                    <div className="contentBox skrollable skrollable-before">
                                                        <h2>
                                                            {this.props.home_page_data.Block2.BLOCK2_TITLE2.split(" ").length >= 2 &&(
                                                                <>
                                                                    <span>{this.props.home_page_data.Block2.BLOCK2_TITLE2.split(" ")[0] == ""  ? this.props.home_page_data.Block2.BLOCK2_TITLE2.split(" ")[1] : this.props.home_page_data.Block2.BLOCK2_TITLE2.split(" ")[0]}</span>
                                                                    <span>{this.props.home_page_data.Block2.BLOCK2_TITLE2.split(" ")[0] == ""  ? this.props.home_page_data.Block2.BLOCK2_TITLE2.split(" ")[2] : this.props.home_page_data.Block2.BLOCK2_TITLE2.split(" ")[1]}</span>
                                                                </>    
                                                            )}
                                                            {this.props.home_page_data.Block2.BLOCK2_TITLE2.split(" ").length < 2 && (
                                                              <span>{this.props.home_page_data.Block2.BLOCK2_TITLE2}</span>  
                                                            )}
                                                        </h2>
                                                        <a href={this.props.home_page_data.Block2.BLOCK2_URL2} className="arrowLink">
                                                            <FormattedMessage id="Shop.Collection.Msg" defaultMessage="Shop Collection" />
                                                        </a>
                                                    </div>
                                                </div>
                                                <Background>
                                                    <img className="block2_banner2 hideInMobile" src={this.props.home_page_data.Block2.BLOCK2_BANNER2} />
                                                    
                                                </Background>
                                            </Parallax>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col col-12 apex-col-auto homeBlock">
                            <div id="R33787634367169813" className="homePage">
                                <section className="braPanty">
                                    <div className="wrap">
                                        <div className="sectionBlocks bras observer animate">
                                            <div className="Block2Section1">
                                                <Parallax strength={380}>
                                                    <div className="SectionHeightBra">
                                                        <div className="textBox skrollable skrollable-before">
                                                            <h6>{this.props.home_page_data.Block3.BLOCK3_TITLE1}</h6>
                                                            <a href={this.props.home_page_data.Block3.BLOCK3_URL1} className="arrowLink">
                                                                <FormattedMessage id="Shop.Collection.Msg" defaultMessage="Shop Collection" />
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <Background>
                                                        <img className="block3_banner1 hideInMobile" src={this.props.home_page_data.Block3.BLOCK3_BANNER1} />
                                                        
                                                        <img className="asset1 Flower" src={this.props.home_page_data.Block3.BLOCK3_FLOWER_BANNER} />
                                                    </Background>
                                                </Parallax>
                                            </div>
                                        </div>
                                        <div className="sectionBlocks panties observer animate">
                                            <div className="Block2Section2">
                                                <Parallax strength={380}>
                                                    <div className="SectionHeight">
                                                        <div className="textBox skrollable skrollable-before">
                                                            <h6>{this.props.home_page_data.Block3.BLOCK3_TITLE2}</h6>
                                                            <a href={this.props.home_page_data.Block3.BLOCK3_URL2} className="arrowLink">
                                                                <FormattedMessage id="Shop.Collection.Msg" defaultMessage="Shop Collection" />
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <Background>
                                                        <img className="asset2 hideInMobile" src={this.props.home_page_data.Block3.BLOCK3_BANNER2} alt="" />
                                                        
                                                    </Background>
                                                </Parallax>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col col-12 apex-col-auto homeBlock">
                            <div id="R33787769572169814" className="homePage">
                                <section className="adore">
                                    <div className="sectionBlocks animate">
                                        <div className="Block3Section">
                                            <Parallax strength={380}>
                                                <div className="SectionLastBlockHeight">
                                                    <div className="contentBox observer adoreBlock skrollable skrollable-before" data-bottom-top="transform:translateY(15%) translateX(-50%);" data-top-bottom="transform:translateY(-15%) translateX(-50%);" style={{ 'transform': 'translateY(15%) translateX(-50%)' }}>
                                                        <h6>
                                                            {this.props.home_page_data.Block4.BLOCK4_TITLE.split(" ").length >= 2 &&(
                                                                <>
                                                                    <span>{this.props.home_page_data.Block4.BLOCK4_TITLE.split(" ")[0] == ""  ? this.props.home_page_data.Block4.BLOCK4_TITLE.split(" ")[1] : this.props.home_page_data.Block4.BLOCK4_TITLE.split(" ")[0]}</span>
                                                                    <span>{this.props.home_page_data.Block4.BLOCK4_TITLE.split(" ")[0] == ""  ? this.props.home_page_data.Block4.BLOCK4_TITLE.split(" ")[2] : this.props.home_page_data.Block4.BLOCK4_TITLE.split(" ")[1]}</span>
                                                                </>    
                                                            )}
                                                            {this.props.home_page_data.Block4.BLOCK4_TITLE.split(" ").length < 2 && (
                                                              <span>{this.props.home_page_data.Block4.BLOCK4_TITLE}</span>  
                                                            )}
                                                        </h6>
                                                        <a href={this.props.home_page_data.Block4.BLOCK4_URL} className="arrowLink">
                                                            <FormattedMessage id="Shop.Collection.Msg" defaultMessage="Shop Collection" />
                                                        </a>
                                                    </div>
                                                </div>
                                                <Background>
                                                    <img className="block4_banner1 hideInMobile" src={this.props.home_page_data.Block4.BLOCK4_BANNER1} />
                                                   
                                                    <img className="imgAsset" src={this.props.home_page_data.Block4.BLOCK4_BANNER2} />
                                                </Background>
                                            </Parallax>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                   */}
                    {/* <div className="row">
                        <div className="col col-12 apex-col-auto homeBlock">
                            <div id="R36275003485418909" className="homePage">
                                <section className="trendingBlock">
                                    <div className="wrap">
                                        <h6>
                                                            <a href="https://www.instagram.com/nayomimena/" target="_blank"><i
                                                                className="icon-instagram"></i><span>{InstaTitle1}</span><span className="PinkText">{InstaTitle2}</span></a></h6>
                                        <div className="trendingList">
                                            <Slider {...settings}>{ this.props.home_page_data.instagram.image_data && this.props.home_page_data.instagram.image_data.map((item, index) => (
                                                <div>
                                                    <figure key={index}>
                                                        <a href={item.a_link} target="_blank">
                                                            <img src={item.image} />
                                                        </a>
                                                    </figure>
                                                </div>)) }</Slider>
                                        </div>
                                    </div>
                                </section>{/*
                                <script src="https://cdnjs.cloudflare.com/ajax/libs/instafeed.js/1.4.1/instafeed.min.js"></script>*/}</div>
                        </div>
                    </div>
               
                </div> 
           

</div>
            </>
        );
    }
}
export default HomePageComponent;