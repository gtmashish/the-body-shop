import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import { Link, withRouter } from 'react-router-dom';

import { connect } from 'react-redux';
import * as actions from '../../../redux/actions/index';
import { initialize, pageview } from '../../utility/googleAnalytis';
import { initializeF, pageViewF } from '../../utility/facebookPixel';
import { initializeGTM, dataLayerGTM } from '../../utility/googleTagManager';
import { live } from '../../../api/globals';
import parse from 'html-react-parser';
import ShowMore from 'react-show-more';
import $ from 'jquery';

import ScrollToTop from 'react-scroll-up';
const style = {
    visibility: 'visible',
    opacity: 0.5,

}

let count = 0;
let loader = true;
class Footer extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            readMore: 'none',
            readMoreMobile: 'none',
            readMoreButtonText: 'Read More',
            readLessButtonText: 'Read less',
            customerService: '8001244443',
            categoryFooter: false
        }
        count = 0;
        loader = true;
    }
    componentDidMount() {
        this.changeCustomerServiceNumber();
        const currentStore = this.props.globals.currentStore;
        let readMoreText = "Read More";
        let readLessText = "Read Less";
        if(currentStore == 1 || currentStore == 3 || currentStore == 5){
            readMoreText = "قراءة المزيد";
            readLessText = "أقرأ أقل";
        }

        this.setState({ readMoreButtonText : readMoreText, readLessButtonText: readLessText })
    }
    componentDidUpdate(prevProps) {
        if (this.props.globals.currentStore !== prevProps.globals.currentStore) {
            this.changeCustomerServiceNumber();
            const currentStore = this.props.globals.currentStore;
            let readMoreText = "Read More";
            let readLessText = "Read Less";
            if(currentStore == 1 || currentStore == 3 || currentStore == 5){
                readMoreText = "قراءة المزيد";
                readLessText = "أقرأ أقل";
            }

        this.setState({ readMoreButtonText : readMoreText, readLessButtonText: readLessText })
        }
        
        if(this.props.location.pathname != prevProps.location.pathname) {
            if(this.props.location.pathname.indexOf('/products/') != -1 && this.props.location.pathname.indexOf('/search') == -1){
                this.setState({categoryFooter : true});
            }else{
                this.setState({categoryFooter : false});
            }
            if(live) {
                initialize();
                pageview();
                initializeF();
                pageViewF();
                initializeGTM();
                dataLayerGTM();
                count = 1;
            }
        }

        if(prevProps.location.pathname ==  prevProps.location.pathname && count == 0) {
            if(loader) {
                if(prevProps.location.pathname.indexOf('/products/') != -1 && this.props.location.pathname.indexOf('/search') == -1){
                    this.setState({categoryFooter : true});
                }else{
                    this.setState({categoryFooter : false});
                }
                loader = false;
            }
            if(live) {
                initialize();
                pageview();
                initializeF();
                pageViewF();
                initializeGTM();
                dataLayerGTM();
            }
        }
    }

    changeCustomerServiceNumber = () => {

        const currentStore = this.props.globals.currentStore;

        if (currentStore === 1) {
            this.setState({
                customerService: '8001244443',
            })
        } else if (currentStore == 2) {
            this.setState({
                customerService: '8001244443',
            })
        } else if (currentStore == 3) {
            this.setState({
                customerService: '97143974173',
            })
        } else if (currentStore == 4) {
            this.setState({
                customerService: '97143974173',
            })
        } else if (currentStore == 5) {
            this.setState({
                customerService: '97143974173',
            })
        } else if (currentStore == 6) {
            this.setState({
                customerService: '97143974173',
            })
        }
    }
    onReadMore = () => {
        const currentStore = this.props.globals.currentStore;
        let readMoreText = "Read More";
        let readLessText = "Read Less";
        if(currentStore == 1 || currentStore == 3 || currentStore == 5){
            readMoreText = "قراءة المزيد";
            readLessText = "أقرأ أقل";
        }
        $(".more-text").toggle("slow");
        if (this.state.readMore === 'none') {
            
            this.setState({
                readMore: 'inline',
                readMoreButtonText: readLessText,
            })
        } else if (this.state.readMore === 'inline') {
            this.setState({
                readMore: 'none',
                readMoreButtonText: readMoreText,

            })
        }
    }
    onReadMoreMobile = () => {
        const currentStore = this.props.globals.currentStore;
        let readMoreText = "Read More";
        let readLessText = "Read Less";
        if(currentStore == 1 || currentStore == 3 || currentStore == 5){
            readMoreText = "قراءة المزيد";
            readLessText = "أقرأ أقل";
        }
        //document.getElementsByClassName("more-text").toggle("slow");
        $(".m-more-text").toggle("slow");
        if (this.state.readMoreMobile === 'none') {
            
            this.setState({
                readMoreMobile: 'inline',
                readMoreButtonText: readLessText,
            })
        } else if (this.state.readMoreMobile === 'inline') {
            this.setState({
                readMoreMobile: 'none',

                readMoreButtonText: readMoreText,
            })
        }
    }

    handleClick = (e) => {

        // return;
        let orignalclassNameMainComponent = 'b-accordion_wrapper navigation_wrapper_1 js-accordion_wrapper';
        let expandedclassNameMainComponent = 'b-accordion_wrapper navigation_wrapper_1 js-accordion_wrapper m-active';

        let orignalclassNameChildComponent = 'b-accordion_container js-accordion_container';
        let expandedclassNameChildComponent = 'b-accordion_container js-accordion_container collapse-a';

        if (!(e.currentTarget.tagName == 'SPAN' || e.currentTarget.tagName == 'LI' || e.currentTarget.tagName == 'A')) {

            if (e.currentTarget.className == orignalclassNameMainComponent) {
                e.currentTarget.className = expandedclassNameMainComponent;
            } else {
                e.currentTarget.className = orignalclassNameMainComponent;
            }

            let a = e.currentTarget.children;
            if (a[1].className == orignalclassNameChildComponent) {
                a[1].className = expandedclassNameChildComponent;
            } else {
                a[1].className = orignalclassNameChildComponent;
            }
        }


    }
    render() {

        // let selected_Store = localStorage.getItem('selectedStore');
        const store_locale = this.props.globals.store_locale;

        const longText = () => {
            return (
                <span className="more-text"><FormattedMessage id="Footer.Line1" defaultMessage="Footer Content" /><p></p>
                    <p><FormattedMessage id="Footer.Line2" defaultMessage="Footer Content" /></p>
                    <p>&nbsp;</p> <p><strong><FormattedMessage id="Footer.Line3" defaultMessage="Footer Content" /> &amp;<FormattedMessage id="Footer.Line4" defaultMessage="Footer Content" /> </strong></p>
                    <p><FormattedMessage id="Footer.Line5" defaultMessage="Footer Content" /></p>
                </span>
            )
        }

        return (
            <>
                <footer className="t-Footer">
                    <div className="t-Footer-body">
                        <div className="t-Footer-content">
                            <div className="container">
                                <div className="row">
                                    <div className="col col-12 apex-col-auto">
                                        <div id="R33787939852169816" className="">
                                            <input type="hidden" id="P0_TOTAL" name="P0_TOTAL" value="SAR 4431" />
                                            <input type="hidden" id="P0_QTY" name="P0_QTY" value="54" />
                                            <input type="hidden" id="P0_STOREID" name="P0_STOREID" value="" />
                                            <input type="hidden" id="P0_TEST" name="P0_TEST" value="" />
                                            <input type="hidden" id="P0_PCOUNTRY" name="P0_PCOUNTRY" value="" />
                                            <input type="hidden" id="P0_PCONT_CODE" name="P0_PCONT_CODE" value="" />
                                            <input type="hidden" id="P0_META_DESC" name="P0_META_DESC" value="Shop lingerie online at best prices on Nayomi. Choose from wide range of bras, panties, nightwear, shapewear &amp; loungewear for women. ✓ Free Delivery* ✓ Click &amp; Collect ✓ Cash on Delivery" />
                                            <input type="hidden" data-for="P0_META_DESC" value="rAbrbbWQioCS6UzcdR_V2745qTeeVwtG7IeGIvgkvOS9qrXdE9RNSloBbUkc5hzbxECzbo5cYTxCq3Fa-4vOQQ" />
                                            <input type="hidden" id="P0_SEO_COUNTRY_DF" name="P0_SEO_COUNTRY_DF" value="https://www.nayomi.com/en/" />
                                            <input type="hidden" data-for="P0_SEO_COUNTRY_DF" value="RNKfN3zdK6-IuHIgyz4_bubcb-ZBjZZpt8ms5mvpfSpCjlRoi0a-vshfKTcePXw5jrTRRJYsl2lu7zkfxqY4mA" />
                                            <input type="hidden" id="P0_META_KW" name="P0_META_KW" value="online shopping,lingerie,bra,sexy lingerie,online shop,women&#39;s underwear,ladies underwear,womens lingerie" />
                                            <input type="hidden" data-for="P0_META_KW" value="y6rNXkDu4NAPaDwowPiWQS8KJy-laEZZnRAa4VErVuhfL56CqOLCbkpI7j1Z0Jcxp-MjjFP5vubRkgDthLgtNA" />
                                            <input type="hidden" id="P0_SEO_AR_AE" name="P0_SEO_AR_AE" value="" />
                                            <input type="hidden" id="P0_SEO_EN_AE" name="P0_SEO_EN_AE" value="https://www.nayomi.com/uae-en/" />
                                            <input type="hidden" data-for="P0_SEO_EN_AE" value="ucRAa63vDo9baT6RtShWk_XGE_HaWrdkwD2aPknjRWznJHBdcXdMYZIZMFhjqdTWMk9fYIxIaT94u01lTglgVw" />
                                            <input type="hidden" id="P0_SEO_AR_SA" name="P0_SEO_AR_SA" value="https://www.nayomi.com/saudi-ar/" />
                                            <input type="hidden" data-for="P0_SEO_AR_SA" value="Ky5KFE-AaFHhTTMkryr39h2aBQJmXWlQ4pnyaZcUnCNmDSJJI35rv5MDkvbb7dVjL2_YUlXpvNiwZTVEIgIxfA" />
                                            <input type="hidden" id="P0_SEO_EN_SA" name="P0_SEO_EN_SA" value="/" />
                                            <input type="hidden" data-for="P0_SEO_EN_SA" value="9JlfMUnksBdXn5rLDziubVyKIrvjBALkMQ7OZrAS2FuOevmMhKzqlLolFqMq4Wk47o0XN5mtM2dRYYOJ5LOcYA" />
                                            <input type="hidden" id="P0_SUB_STY" name="P0_SUB_STY" value="0" />
                                            <input type="hidden" data-for="P0_SUB_STY" value="vl570P6Rysc1PmWzDJbU_SRh3p-sXWBJm3gb5Ad8C4CuIKH8PMLMO_9mAj9LASGNVALVfOe5dewJ6bNRKXPkeQ" />
                                            <input type="hidden" id="P0_SEO_CANONICAL" name="P0_SEO_CANONICAL" value="/" />
                                            <input type="hidden" data-for="P0_SEO_CANONICAL" value="koIulZx9J76C0DMA_UJv3c1G0Vb5XIXt1QqD-jcQ1nVhTGyUu76hUfWeQ1OoPICKL9tsEARHtY46bk_EEwFFyw" />
                                            <input type="hidden" id="P0_BREADCRUMB_HOME" name="P0_BREADCRUMB_HOME" value="" />
                                            <input type="hidden" data-for="P0_BREADCRUMB_HOME" value="riSqtH4cs8JEY862YJoTFLpo4Oefteq2gLsn_rOi8tJ3Cy9di3FPbBBXqbpPv81HyxsdFjLqUk4cHafR-HJW5g" />
                                            <input type="hidden" id="P0_BREADCRUMB_L1" name="P0_BREADCRUMB_L1" value="" />
                                            <input type="hidden" data-for="P0_BREADCRUMB_L1" value="QPDrjPoejbKwXKK5KHMOaCnXPO0QAOqu98iv8ftkBlvuObiky9G71_zPfT8wgK3BLINmS-Xa2DtApy0KUIujbg" />


                                            <footer className="footer">
                                                <div className="row-1">
                                                    <div className="containers">
                                                        <div className="col-1" style={{ width: 'auto' }}>
                                                            <h4><FormattedMessage id="footer.customerService" defaultMessage="Customer Service" /> : 
                                                            <a className="js-ga-tracking" data-ga-category="Contact Us" data-ga-action="click" data-ga-label="Telephone" href={`tel:${this.state.customerService}`}><strong>{this.state.customerService}</strong></a></h4>
                                                            <span className="fch"><FormattedMessage id="footer.connectWithUs" defaultMessage="Connect with us" /></span>
                                                            <div className="media">
                                                                <a href="http://www.instagram.com/nayomimena"><i className="icon-instagram"></i></a>
                                                                <a href="http://www.facebook.com/NayomiMENA"><i className="icon-facebook"></i></a>
                                                                <a href="http://www.youtube.com/NayomiMENA"><i className="icon-youtube"></i></a>
                                                                <a href="https://api.whatsapp.com/send?phone=971565069237"><i className="icon-whatsapp"></i></a>
                                                            </div>
                                                        </div>

                                                        {!this.state.categoryFooter && (<div className="col-2">
                                                            <div leng={740} id="footerContent"><h1 className="h1p"><FormattedMessage id="Footer.Content1" defaultMessage="Footer content" /></h1> 
                                                                <p><FormattedMessage id="Footer.Content2" defaultMessage="Footer content" /></p>  <p><strong /></p>

                                                                <span className="more-text">
                                                                    <FormattedMessage id="Footer.Content3" defaultMessage="Footer content" />
                                                                    <p /> 
                                                                    <p>
                                                                        <FormattedMessage id="Footer.Content4" defaultMessage="Footer content" />
                                                                    </p>
                                                                    <p>&nbsp;</p>
                                                                    <p>
                                                                        <strong>
                                                                            <FormattedMessage id="Footer.Content6" defaultMessage="Footer content" />
                                                                        </strong>
                                                                    </p>
                                                                    <p>
                                                                        <FormattedMessage id="Footer.Content7" defaultMessage="Footer content" />
                                                                    </p>
                                                                </span>
                                                            </div>
                                                            <a className="moreless-button" onClick={this.onReadMore}>{this.state.readMoreButtonText}...</a><p className="cp"><FormattedMessage id="Footer.Content5" defaultMessage="Footer content" /></p>
                                                        </div>)}
                                                        {this.state.categoryFooter && (
                                                            <div className="col-2">
                                                                {this.props.productDetails.category_description &&(
                                                                    
                                                                      
                                                                        <div>
                                                                            {parse(this.props.productDetails.category_description)} 
                                                                            <a className="moreless-button" onClick={this.onReadMore}>{this.state.readMoreButtonText}...</a>
                                                                            <p className="cp">
                                                                                <FormattedMessage id="Footer.Content5" defaultMessage="Footer content" />
                                                                            </p> 
                                                                        </div>
                                                                   
                                                                )}
                                                            </div>
                                                        )}

                                                        <div className="col-3" style={{ width: 'auto' }}>
                                                            <h4><Link to={`/${store_locale}/payment-methods`}><FormattedMessage id="footer.paymentMethodsAll" defaultMessage="Payment Methods" /></Link></h4>
                                                            <Link to={`/${store_locale}/payment-methods`}><figure></figure></Link>
                                                        </div>
                                                    </div>
                                                </div>
                                            </footer>
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col col-12 apex-col-auto">
                                        <div id="R16194527186257339" style={{ background: '#fde9ed' }}>
                                            <div className="b-footer_middle">
                                                <div className="g-wrapper-main_content">
                                                    <div className="b-footer_navigation">
                                                        <div className="content-asset">

                                                            <div className="b-accordion_wrapper navigation_wrapper_1 js-accordion_wrapper" id="abt" onClick={((e) => this.handleClick(e))}  >
                                                                <h6 className="b-accordion_title js-accordion_title icon-down-footer"><FormattedMessage id="footer.aboutTitle" defaultMessage="About Nayomi" /></h6>
                                                                <ul className="b-accordion_container js-accordion_container">
                                                                    <li>
                                                                        <Link to={`/${store_locale}/brand-overview`} style={{ textDecoration: 'none' }}>
                                                                            <FormattedMessage id="footer.brandOverview" defaultMessage="Brand Overview" />
                                                                        </Link>
                                                                    </li>
                                                                    <li><a href="https://kojhr.com/en/careers/main/home.aspx" title="Careers" traget="_blank"><FormattedMessage id="footer.Careers" defaultMessage="Careers" /></a></li>
                                                                </ul>
                                                            </div>

                                                            <div className="b-accordion_wrapper navigation_wrapper_1 js-accordion_wrapper" id="abt" onClick={((e) => this.handleClick(e))}>
                                                                <h6 className="b-accordion_title js-accordion_title icon-down-footer"><FormattedMessage id="footer.needAssistance" defaultMessage="Need Assistance ?" /></h6>
                                                                <ul className="b-accordion_container js-accordion_container">
                                                                    <li>
                                                                        <Link to={`/${store_locale}/help`} style={{ textDecoration: 'none' }}>
                                                                            <FormattedMessage id="footer.helpCenter" defaultMessage="Help Center" />
                                                                        </Link>
                                                                    </li>
                                                                    <li>
                                                                        <Link to={`/${store_locale}/contact-us`} style={{ textDecoration: 'none' }}>
                                                                            <FormattedMessage id="footer.contactUs" defaultMessage="Contact Us" />
                                                                        </Link>
                                                                    </li>
                                                                    <li>
                                                                        <Link to={`/${store_locale}/delivery`} style={{ textDecoration: 'none' }}>
                                                                            <FormattedMessage id="footer.delivery" defaultMessage="Delivery" />
                                                                        </Link>
                                                                    </li>
                                                                    <li><Link to={`/${store_locale}/faq`} style={{ textDecoration: 'none' }}><FormattedMessage id="footer.faq" defaultMessage="FAQ" /></Link></li>
                                                                    <li><Link to={`/${store_locale}/returns-and-exchanges`} style={{ textDecoration: 'none' }}>
                                                                        <FormattedMessage id="footer.returnandexchanges" defaultMessage="Returns and Exchanges" />
                                                                    </Link></li>
                                                                    <li><Link to={`/${store_locale}/privacy-policy`} style={{ textDecoration: 'none' }}>
                                                                        <FormattedMessage id="footer.privacyPolicy" defaultMessage="Privacy Policy" /></Link></li>
                                                                    <li>
                                                                        <Link to={`/${store_locale}/terms-and-conditions`} style={{ textDecoration: 'none' }}>
                                                                            <FormattedMessage id="footer.termsAndConditions" defaultMessage="Terms And Conditions" /></Link></li>
                                                                    <li>
                                                                        <Link to={`/${store_locale}/payment-methods`} style={{ textDecoration: 'none' }}><FormattedMessage id="footer.paymentMethods" defaultMessage="Payment Methods" /></Link></li>
                                                                </ul>
                                                            </div>

                                                            <div className="b-accordion_wrapper navigation_wrapper_1 js-accordion_wrapper" id="abt" onClick={((e) => this.handleClick(e))}>
                                                                <h6 className="b-accordion_title js-accordion_title icon-down-footer"><FormattedMessage id="footer.storeLocator" defaultMessage="Store Locator" /></h6>
                                                                <ul className="b-accordion_container js-accordion_container">
                                                                    <li>
                                                                        <Link to={`/${store_locale}/store-locator-uae`} style={{ textDecoration: 'none' }}>
                                                                            <FormattedMessage id="footer.uae" defaultMessage="United Arab Emirates" />
                                                                        </Link>
                                                                    </li>
                                                                    <li>
                                                                        <Link to={`/${store_locale}/store-locator-saudi`} title="Saudi Arabia" style={{ textDecoration: 'none' }}>
                                                                            <FormattedMessage id="footer.sa" defaultMessage="Saudi Arabia" />
                                                                        </Link>
                                                                    </li>
                                                                    <li><Link to={`/${store_locale}/store-locator-kuwait`} style={{ textDecoration: 'none' }} title="Kuwait"><FormattedMessage id="footer.kuwait" defaultMessage="Kuwait" /></Link></li>
                                                                    <li><Link to={`/${store_locale}/store-locator-bahrain`} style={{ textDecoration: 'none' }} title="Bahrain"><FormattedMessage id="footer.bahrain" defaultMessage="Bahrain" /></Link></li>
                                                                    <li><Link to={`/${store_locale}/store-locator-qatar`} style={{ textDecoration: 'none' }} title="Qatar"><FormattedMessage id="footer.qatar" defaultMessage="Qatar" /></Link></li>
                                                                    <li><Link to={`/${store_locale}/store-locator-oman`} style={{ textDecoration: 'none' }} title="Oman"><FormattedMessage id="footer.oman" defaultMessage="Oman" /></Link></li>
                                                                    <li><Link to={`/${store_locale}/store-locator-morocco`} style={{ textDecoration: 'none' }} title="Morocco"><FormattedMessage id="footer.morocco" defaultMessage="Morocco" /></Link></li>

                                                                </ul>
                                                            </div>



                                                            <div className="b-accordion_wrapper navigation_wrapper_1 js-accordion_wrapper" id="abt" onClick={((e) => this.handleClick(e))}>
                                                                <h6 className="b-accordion_title js-accordion_title icon-down-footer"><FormattedMessage id="footer.amirahClubSignUp" defaultMessage="Amirah Club Sign Up" /></h6>
                                                                <ul className="b-accordion_container js-accordion_container">
                                                                    <li><div style={{ display: 'flex' }} className="en"> <Link to={`/${store_locale}/amirah-club`} style={{ textDecoration: 'none' }} className="Acregister"></Link> </div></li>
                                                                </ul>
                                                            </div>

                                                            <div className="b-accordion_wrapper navigation_wrapper_1 js-accordion_wrapper" id="abt" onClick={((e) => this.handleClick(e))}>
                                                                <h6 className="b-accordion_title js-accordion_title icon-down-footer"><FormattedMessage id="footer.appDownload" defaultMessage="App Download" /></h6>
                                                                <ul className="b-accordion_container js-accordion_container">
                                                                    <li><div style={{ display: 'flex' }} className="en"> <a href="https://play.google.com/store/apps/details?id=prototype.nayomi.app" className="Applogo"></a> </div></li>
                                                                    <li><div style={{ display: 'flex' }} className="en"> <a href="https://itunes.apple.com/app/nayomi-lingerie/id965168348?mt=8" className="Appstore"></a> </div></li>
                                                                </ul>
                                                            </div>

                                                            <div className="b-accordion_wrapper navigation_wrapper_4 js-accordion_wrapper"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col col-12 apex-col-auto">
                                        <div id="R33788277265169819" style={{ background: '#fde9ed' }} className="h-hidden-desktop  t-Form--slimPadding">
                                            <footer className="footer">
                                                <div className="row-1">
                                                    <div className="containers" style={{ marginleft: '20px', marginright: '20px' }}>
                                                        <div className="col-1" style={{ width: 'auto', display: 'none' }}>
                                                            <h4>Customer Service : <a className="js-ga-tracking" data-ga-category="Contact Us" data-ga-action="click" data-ga-label="Telephone" href={`tel:${this.state.customerService}`}><strong>{this.state.customerService}</strong></a></h4>
                                                            <span className="fch"><FormattedMessage id="footer.connectWithUs" defaultMessage="Connect with us" /></span>
                                                            <div className="media">
                                                                <a href="http://www.instagram.com/nayomimena"><i className="icon-instagram"></i></a>
                                                                <a href="http://www.facebook.com/NayomiMENA"><i className="icon-facebook"></i></a>
                                                                <a href="http://www.youtube.com/NayomiMENA"><i className="icon-youtube"></i></a>
                                                                <a href="https://api.whatsapp.com/send?phone=971565069237"><i className="icon-whatsapp"></i></a>
                                                            </div>
                                                        </div>

                                                        {!this.state.categoryFooter && (<div className="col-12" style={{ display: 'table', margin: '3px auto', marginBottom: "20px" }}>
                                                            <div leng="237" id="mfooterContent">
                                                                <h1 className="h1p">
                                                                    <FormattedMessage id="MobileFooter.Content1" defaultMessage="Footer Content" />
                                                                </h1> 
                                                                <p>
                                                                    <FormattedMessage id="MobileFooter.Content2" defaultMessage="Footer content" />
                                                                </p> 
                                                                <p><strong /></p>
                                                            </div>
                                                            <div>
                                                                <span className="m-more-text">
                                                                    <FormattedMessage id="MobileFooter.Content3" defaultMessage="Footer content" />
                                                                    <p /> 
                                                                    <p>
                                                                        <FormattedMessage id="MobileFooter.Content4" defaultMessage="Footer content" />
                                                                    </p>
                                                                    <p>
                                                                        <FormattedMessage id="MobileFooter.Content4" defaultMessage="Footer content" />
                                                                    </p>
                                                                    <p>
                                                                        <FormattedMessage id="MobileFooter.Content5" defaultMessage="Footer content" />
                                                                    </p>
                                                                    <p>&nbsp;</p>
                                                                    <p>
                                                                        <strong><FormattedMessage id="MobileFooter.Content7" defaultMessage="Footer content" /></strong>
                                                                    </p>
                                                                    <p>
                                                                        <FormattedMessage id="MobileFooter.Content8" defaultMessage="Footer content" />
                                                                    </p>
                                                                </span>
                                                            </div>
                                                            <div>  
                                                                <a className="m-moreless-button" onClick={this.onReadMoreMobile}>{this.state.readMoreButtonText}...</a></div>
                                                                <p className="cp"><FormattedMessage id="MobileFooter.Content6" defaultMessage="Footer content" /></p>
                                                            </div>)}

                                                         {this.state.categoryFooter && (
                                                            <div className="col-12" style={{ display: 'table', margin: '3px auto', marginBottom: "20px" }}>
                                                                <div leng="237" id="mfooterContent">
                                                                    {this.props.productDetails.category_description &&(
                                                                        <div>
                                                                            {parse(this.props.productDetails.category_description)}
                                                                            <a className="m-moreless-button" onClick={this.onReadMore}>
                                                                            {this.state.readMoreButtonText}...</a>
                                                                            <p className="cp">
                                                                                <FormattedMessage id="Footer.Content5" defaultMessage="Footer content" />
                                                                            </p>  
                                                                        </div>
                                                                    )}
                                                                </div>    
                                                            </div>
                                                        )}
                                                            
                                                            <div className="col-3" style={{ width: 'auto', display: 'none' }}>
                                                            <h4><Link to={`/${store_locale}/payment-methods`}><FormattedMessage id="footer.paymentMethods" defaultMessage="Payment Methods" /></Link></h4>
                                                            <figure></figure>
                                                        </div>
                                                    </div>
                                                </div>

                                            </footer>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </footer>

                <ScrollToTop showUnder={10} duration={550}>
                    <a className="cd-top js-cd-top cd-top--fade-out cd-top--show" style={style}>Top</a>
                </ScrollToTop>

            </>
        );
    }
}

// export default Footer;

const mapStateToProps = state => {
    return {
        globals: state.global,
        productDetails: state.productDetails,
    }
}

export default withRouter(connect(mapStateToProps)(Footer));