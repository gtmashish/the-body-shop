import React, { Component } from 'react';
import Axios from 'axios';
import { withRouter } from 'react-router-dom';

import Cart from './Cart';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import OfferStripe from '../OfferStripe/OfferStripe';

import MenuNav from '../../Menu/menuNav';
import Search from '../Search/Search';
import * as utility from '../../utility/utility';
import { connect } from 'react-redux';
import * as actions from '../../../redux/actions/index';
import cookie from 'react-cookies';
import amirahclub from '../../../../assets/images/amirahclub_mobile.png';

import { Helmet } from 'react-helmet';
import { WEB_URL } from '../../../api/globals';

import logo from '../../../../assets/images/logo/logo.png';


class MainHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showCountries: false,
            country_flag: '',
            showCart: false,
            cartItemCount: 3
        }
    }

    showCart = () => {
        this.setState({
            showCart: !this.state.showCart
        })
    }

    componentDidMount() {
        this.props.onGetStoreIds();
        //console.log('In componentDidMount before onGetMenuNav', this.props.global);
        //this.props.onGetMenuNav(this.props.globals);

        // if (this.props.globals.currentStore) {
        //     this.props.onGetMenuNav(this.props.globals);
        // }

        if (this.props.countryList.length == 0) {
            this.props.onGetCountryList();
        }


        let country = (cookie.load('country') === null) ? 'KSA' : cookie.load('country');
        this.setState({ country_flag: this.getFlagName(country) });
        this.getStore();
        this.props.onGetStoreList({
            country_id: '',
            city: ''
        });
    }

    componentDidUpdate(prevProps, prevState) {

        if (this.props.location.pathname !== prevProps.location.pathname) {
            // console.log('Route change!');
            this.setState({ showCountries: false })
            if (this.state.showCart) {
                console.log(this.state.showCart)
                this.setState({
                    showCart: false
                })
            }
        }

        if (prevProps.globals.currentStore !== this.props.globals.currentStore) {
            console.log('Calling from  update :: ', prevProps.globals.currentStore, this.props.globals.currentStore)
            this.props.onGetMenuNav(this.props.globals);
        }

        if ((this.props.guest_user.temp_quote_id !== prevProps.guest_user.temp_quote_id) || (!(utility.isEquivalent(this.props.user_details.customer_details, prevProps.user_details.customer_details)))) {
            this.getStore();
        }

        if (prevProps.globals.country !== this.props.globals.country) {
            this.setState({ country_flag: this.getFlagName(this.props.globals.country) });
        }
    }

    getStore = () => {
        let obj = this.props.user_details.customer_details;

        if (!(utility.emptyObj(obj))) {
            if (!(this.props.cart_details.is_cart_details_rec)) {
                this.props.onGetMyCart({ quote_id: this.props.user_details.customer_details.quote_id, store_id: this.props.globals.currentStore })
            }
        } else if (utility.emptyObj(obj)) {
            if (this.props.guest_user.temp_quote_id == null) {
                this.props.onGetGuestCartId();
            }

            if (this.props.guest_user.new_quote_id !== null) {
                if (!(this.props.cart_details.is_cart_details_rec)) {
                    this.props.onGetMyCart({ quote_id: this.props.guest_user.new_quote_id, store_id: this.props.globals.currentStore })
                }
            }
        }

    }

    showCountries = () => {
        this.setState({ showCountries: !this.state.showCountries })
    }

    translate = (lang, dir) => {
        this.props.handleLanguageSelection(lang, dir);
    }

    onChangeCountry = (country) => {
        this.props.handleCountrySelection(country);
        this.setState({ country_flag: this.getFlagName(country) });
        // console.log('onChangeCountry', this.state.country_flag);
        this.showCountries();
        this.closeHBMenu();
    }

    getFlagName(country) {
        var flag_name;

        switch (country) {
            case 'KSA':
                flag_name = 'ksa';
                break;
            case 'UAE':
                flag_name = 'uae';
                break;
            case 'International':
                flag_name = 'usa';
                break;
            default:
                flag_name = 'ksa';
        }
        return flag_name;
    }

    handleDir = (language) => {
        if (language === 'ar') {
            document.getElementById("dir").classList.add("u-RTL");
            document.getElementById("dir").lang = 'ar';
            document.getElementById("dir").dir = 'rtl';
        } else {
            document.getElementById("dir").lang = 'en';
            document.getElementById("dir").classList.remove("u-RTL");
            document.getElementById("dir").removeAttribute('dir');
        }
    }

    closeHBMenu = () => {
        document.querySelector("html").classList.remove("menuOpen");
    }

    render() {
        //console.log('props from header ', this.props);
        //console.log('state from header ',this.state);

        const store_locale = this.props.globals.store_locale;

        const cartDetails = <Cart showCart={this.state.showCart}
            cartDetails={this.props.cart_details}
            toggleCart={this.showCart}
            store_locale={this.props.globals.store_locale}
        />;

        let profileIcon = null
        if (!(this.props.user_details.isUserLoggedIn)) {
            profileIcon = <li><Link to={`/${store_locale}/login`} className="whatsapp">
                <i className="icon-user"></i>
                <strong id="LGUSRUQ"> </strong>
                <span style={{ whiteSpace: 'nowrap' }}><FormattedMessage id="header.Profile" defaultMessage="Profile" /></span>
            </Link>
            </li>

        } else {

            profileIcon = <li><Link to={`/${store_locale}/profile`} className="whatsapp">
                <i className="icon-user"></i>
                <strong id="LGUSRUQ">{this.props.user_details.customer_details.firstname}</strong>
                <span style={{ whiteSpace: 'nowrap' }}><FormattedMessage id="header.Profile" defaultMessage="Profile" /></span>
            </Link>
            </li>

        }
        //console.log(this.props.guest_user);
        // console.log('render', this.state.country_flag);
        return (
            <>
                <Helmet>
                    <link rel="alternate" hreflang="x-default" href={`${WEB_URL}en/`} />
                    <link rel="alternate" hreflang="ar-AE" href={`${WEB_URL}uae-ar/`} />
                    <link rel="alternate" hreflang="en-AE" href={`${WEB_URL}uae-en/`} />
                    <link rel="alternate" hreflang="ar-SA" href={`${WEB_URL}saudi-ar/`} />
                    <link rel="alternate" hreflang="en-SA" href={`${WEB_URL}saudi-en/`} />
                    <link rel="canonical" href={`${WEB_URL}saudi-en/`} />
                </Helmet>

                <header className="header" id="t_Header">
                    <div className="t-Header-branding">
                        <div className="row-1">
                            <div className="containers-main">
                                <ul className="leftLink">
                                    <li><Link to={`/${store_locale}`}>
                                        <FormattedMessage id="header.freedelivery" />
                                    </Link></li>
                                    <li><Link to={`/${store_locale}`}>
                                        <FormattedMessage id="header.returnonline"  />
                                    </Link></li>

                                   
                                </ul >
                                <ul className="rightLink">
                                <li><Link to={`/${store_locale}`}>
                                        <FormattedMessage id="header.signin"  />
                                    </Link></li>
                                <li><Link to={`/${store_locale}`}>
                                 <FormattedMessage id="header.search"  />
                                 <i style={{fontSize:".9em",padding:"5px"}} class="icon-search"></i> 
                                    </Link></li>
                                <li><Link to={`/${store_locale}`}>
                                        <FormattedMessage id="header.newsletter"  />
                                    </Link></li>
                                <li><Link to={`/${store_locale}`}>
                                        <FormattedMessage id="header.wishlist"  />
                                    </Link></li> 
                                      </ul>

                            </div >
                        </div >
                        <div className="t-Header-navBar"></div>
                    </div >

                    {cartDetails}

                    < div id="R33786692346169804" className="row-2" >
                        <input type="hidden" id="P0_HSEARCH" name="P0_HSEARCH" value="" />
                        <div className="mobileshow-upper dark small" >
                               <FormattedMessage id = "header.freedelivery"></FormattedMessage></div> 
                        <div className="containers-main">
                            
            
                            <span className="logos"> 
                              <img className="header-logo" src={logo}/>   </span>
                             
                        </div>
                    </div >
                        
                                            
                    <div className="mobileshow"><p>
                    <i id="navTrigger" class="ico icon-padding fa fa-bars"></i>
                        {/* <i id="navTrigger " class="icon-padding icon-nav"  aria-hidden="true"> </i> */}
                        <i class="icon-search icon-padding  "  aria-hidden="true"></i>
                        <i class="icon-marker icon-padding "  aria-hidden="true"></i>
                        <i class="icon-cart icon-padding" aria-hidden="true"></i>
                        </p></div>
<div id="R33786937309169806" className="menuOverlay"> </div>
                    <div id="R33786847982169805" className="row-3">
                      
                  
                        <a to="JavaScript:;" id="closeNav" className="closeNav">X</a>
                        <div className="containers-main">
                        <i class="icon-search icon-right"  aria-hidden="true"></i>
                        <i class="icon-marker icon-right-user icon-right"  aria-hidden="true"></i>
                        <i class="icon-cart icon-right" aria-hidden="true"></i>
                   
                        {/* <div style={{float:"right"}}>navigation</div>  */}
                            <MenuNav />


                            
                            <div className="mobileCTA">
                                <Link to={`/${store_locale}/store-locator`} ><i className="icon-marker" onClick={this.closeHBMenu}></i></Link>
                                <Link to={`/${store_locale}/amirah-club`} className="amirah-club" onClick={this.closeHBMenu}><img src={amirahclub} /></Link>
                                {/* <Link to={`/${store_locale}/sign-in`} onClick={this.closeHBMenu}><i className="icon-heart"></i></Link> */}
                                <Link to={`/${store_locale}/add-wishlist`} onClick={this.closeHBMenu}><i className="icon-heart"></i></Link>
                                <a className="js-ga-tracking" data-ga-category="Contact Us" data-ga-action="click" data-ga-label="Telephone" href="tel:+971565069237"><i className="icon-whatsapp"></i></a>
                            </div>
                        </div>
                    </div>
                    {/* <div id="R39731766560788077" className="offerStripe">
                        <OfferStripe OfferMessage={this.props.OfferMessage} />
                    </div> */}
                </header >


            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        cart_details: state.myCart,
        user_details: state.login,
        change_pass: state.login.changePasswordDetails,
        addressBook: state.address.addressBook,
        countryList: state.address.countryList,
        addressResp: state.address.addressResp,
        isAddBookRec: state.address.isAddBookRec,
        globals: state.global,
        guest_user: state.guest_user,
        menu: state.menu.menuNavData,
        OfferMessage: state.menu.OfferMessage,

    }
}

const mapDispatchToProps = dispatch => {
    return {
        onGetStoreIds: () => dispatch(actions.getStoreIds()),
        onGetMyCart: (quoteId) => dispatch(actions.getMyCart(quoteId)),
        onGetCountryList: () => dispatch(actions.getCountryList()),
        onGetGuestCartId: () => dispatch(actions.getGuestCartId()),
        onGetMenuNav: (payload) => dispatch(actions.getMenuNav(payload)),
        onGetStoreList: (payload) => dispatch(actions.getStoreList(payload)),

    }

}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MainHeader));
// export default connect(mapStateToProps, mapDispatchToProps)(MainHeader);